<?php
namespace BrewBuddy\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Brewery
 * @package BrewBuddy\MainBundle\Entity
 * @ORM\Entity()
 */
class Brewery
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Brewery", mappedBy="brewery")
     */
    private $brews;

    public function __construct()
    {
        $this->brews = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setBrews($brews)
    {
        $this->brews = $brews;
    }

    public function getBrews()
    {
        return $this->brews;
    }

}
