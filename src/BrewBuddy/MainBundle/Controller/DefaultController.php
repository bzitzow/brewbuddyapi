<?php
namespace BrewBuddy\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }

    /**
     * @Route("/breweries", name="mainbundle_breweries")
     */
    public function breweriesAction(Request $request)
	{

		$m = $this->get('doctrine')->getManager();
		$all = $m->getRepository('BrewBuddyMainBundle:Brewery')->findAll();

		return new JsonResponse($all, 200);

		// echo json_encode($all);

	}

	/**
     * @Route("/locations", name="mainbundle_locations")
     */
	public function locationsAction(Request $request)
	{
		$m = $this->get('doctrine')->getManager();
        $all = $m->getRepository('BrewBuddyMainBundle:Location')->findAll();

		return new JsonResponse($all, 200);
	}

}
