<?php
namespace BrewBuddy\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBreweryData implements FixtureInterface, OrderedFixtureInterface
{
    //  $this->addReference('admin-group', $groupAdmin);
    public function load(ObjectManager $manager)
    {

    }

    public function getOrder()
    {
        return 2;
    }
}
