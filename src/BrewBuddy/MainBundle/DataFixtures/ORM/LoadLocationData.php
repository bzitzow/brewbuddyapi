<?php
namespace BrewBuddy\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BrewBuddy\MainBundle\Entity\Location;

class LoadLocationData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $location = new Location();
        $location->setName('Hacker Lab');
        $location->setLatitude('38.579322');
        $location->setLongitude('-121.482761');
        $location->setCity('Sacramento');
        $location->setAddress('12345 I St.');
        $location->setState('CA');
        $location->setZip(95628);

        $manager->persist($location);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
